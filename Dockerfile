#frontend stage
FROM node:alpine
WORKDIR /usr/src/app/
RUN mkdir /vue
WORKDIR /usr/src/app/vue
COPY node/vue/ .
RUN npm install
RUN npm run build
# backend stage
FROM python:3.7.4
ENV PYTHONBUFFERED 1
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
RUN mkdir /assets
WORKDIR /usr/src/app/assets
COPY node/assets/ .
RUN mkdir /core
WORKDIR /usr/src/app/core
COPY node/core/ .
RUN mkdir /node
WORKDIR /usr/src/app/node
COPY node/node/ .
RUN mkdir /nodeapi
WORKDIR /usr/src/app/nodeapi
COPY node/nodeapi/ .
RUN mkdir /templates
WORKDIR /usr/src/app/templates
COPY node/templates/ .
RUN mkdir /users
WORKDIR /usr/src/app/users
COPY node/users/ .
WORKDIR /usr/src/app
COPY node/db.sqlite3 .
COPY node/manage.py .
