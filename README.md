# Python Blockchain | Django | Django REST Framework | Vue.js

Clone the repo.

Install Django dependencies into your virtual Python environment: ```$ pip install -r requirements.txt```\
Install Vue.js dependencies: ```$ cd node/vue $ npm install```

Start the Vue development server in one terminal: ```$ cd node/vue``` ```$ npm run serve```\
Start the Django Vue development server in another terminal: ```$ cd node``` ```$ python3 manage.py runserver```

To see a RabbitMQ consumer in action in a third terminal, you can optionally perform: ```$ cd node/nodeapi``` ```$ python3 subscriber.py```

For this demo, we will not be setting up a peer network to keep things simple.

```localhost:8000``` (Django dev server default)\
Create an Account

Click ```Create new Wallet``` tab.\
Click ```Mine Coins``` to start with 10 coins in the kitty.\
Click ```Load Blockchain``` to see the blockchain list of block transactions beginning with the genesis block.\
Enter your first name in the ```Recipient Key``` field. Enter an amount of coins to transact. Click ```Send```.\
Click on ```Open Transactions``` and ```Load Transactions``` to view all open transactions.\
Click ```BlockChain``` and ```Mine Coins``` to commit all open transactions to the blockchain. Funds are increased with a mining reward of 10 coins.\
Click ```Load Blockchain``` to see the updated blockchain list of block transactions.\
Refresh the page and click ```Load Wallet.```

The front-end UI interface is created in Vue.js and Bootstrap 4 with an Axios dependency for API endpoint requests. The back-end is developed in Django and Django Rest framework with CSRF token security and user authentication.

Blockchain code using Flask API written by Maximilian Schwarzmüller: <https://www.udemy.com/course/learn-python-by-building-a-blockchain-cryptocurrency/>

I performed the Django / Vue.js integration and built the Django REST framework functionality to expose the API endpoints with both session and token authentication.
