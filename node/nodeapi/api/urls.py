from django.urls import path
from nodeapi.api.views import *


urlpatterns = [
    path('balance/', GetBalanceAPIView.as_view(), name='get-balance'),
    path('broadcast-block/', BroadcastBlockAPIView.as_view()),
    path('broadcast-transaction/', BroadcastTransactionAPIView.as_view()),
    path('chain/', GetChainAPIView.as_view()),
    path('mine/', MineTransactionsAPIView.as_view(), name='mine-block'),
    path('node/', AddNodeAPIView.as_view()),
    path('node/<str:node_url>/', RemoveNodeAPIView.as_view()),
    path('nodes/', GetNodesAPIView.as_view()),
    path('resolve-conflicts/', ResolveConflictsAPIView.as_view()),
    path('transaction/', AddTransactionAPIView.as_view(), name='add-transaction'),
    path('transactions/', GetOpenTransactionAPIView.as_view()),
    path('wallet/', WalletAPIView.as_view(), name='wallet-keys')
]
