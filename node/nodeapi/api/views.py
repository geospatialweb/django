from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from nodeapi.blockchain import Blockchain
from nodeapi.node_id import get_node_id
from nodeapi.wallet import Wallet


node_id = get_node_id()
wallet = Wallet(node_id)
blockchain = Blockchain(wallet.public_key, node_id)


class WalletAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        if wallet.load_keys():
            blockchain = Blockchain(wallet.public_key, node_id)
            response = {
                'public_key': wallet.public_key,
                'private_key': wallet.private_key,
                'funds': blockchain.get_balance()
            }
            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Did you create a new Wallet first?'
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        wallet.create_keys()
        if wallet.save_keys():
            blockchain = Blockchain(wallet.public_key, node_id)
            response = {
                'public_key': wallet.public_key,
                'private_key': wallet.private_key,
                'funds': blockchain.get_balance()
            }
            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Saving the keys failed.'
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetBalanceAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        balance = blockchain.get_balance()
        if balance is not None:
            response = {
                'message': 'Fetched balance successfully.',
                'funds': balance
            }
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Loading balance failed.',
                'wallet_set_up': wallet.public_key is not None
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class BroadcastTransactionAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        values = request.data
        if not values:
            return Response({'error': {
                'code': 400,
                'message': 'No data found.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        required_fields = ['sender', 'recipient', 'amount', 'signature']
        if not all(key in values for key in required_fields):
            return Response({'error': {
                'code': 400,
                'message': 'Some data is missing..'
            }}, status=status.HTTP_400_BAD_REQUEST)
        success = blockchain.add_transaction(values['recipient'],
                                             values['sender'],
                                             values['signature'],
                                             values['amount'],
                                             is_receiving=True)
        if success:
            response = {
                'message': 'Successfully added transaction.',
                'transaction': {
                    'sender': values['sender'],
                    'recipient': values['recipient'],
                    'amount': values['amount'],
                    'signature': values['signature']
                }
            }
            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Creating a transaction failed.'
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class BroadcastBlockAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        values = request.data
        if not values:
            return Response({'error': {
                'code': 400,
                'message': 'No data found.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        if 'block' not in values:
            return Response({'error': {
                'code': 400,
                'message': 'Some data is missing.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        block = values['block']
        if block['index'] == blockchain.chain[-1].index + 1:
            if blockchain.add_block(block):
                response = {'message': 'Block added'}
                return Response(response, status=status.HTTP_201_CREATED)
            else:
                return Response({'error': {
                    'code': 409,
                    'message': 'Block seems invalid.'
                }}, status=status.HTTP_409_CONFLICT)
        elif block['index'] > blockchain.chain[-1].index:
            blockchain.resolve_conflicts = True
            response = {
                'message': 'Blockchain seems to differ from local blockchain.'
            }
            return Response(response, status=status.HTTP_200_OK)
        else:
            message = 'Blockchain seems to be shorter, block not added'
            return Response({'error': {
                'code': 409,
                'message': message
            }}, status=status.HTTP_409_CONFLICT)


class AddTransactionAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        if wallet.public_key is None:
            return Response({'error': {
                'code': 400,
                'message': 'No wallet set up.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        values = request.data
        if not values:
            return Response({'error': {
                'code': 400,
                'message': 'No data found.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        required_fields = ['recipient', 'amount']
        if not all(field in values for field in required_fields):
            return Response({'error': {
                'code': 400,
                'message': 'Some data is missing.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        recipient = values['recipient']
        amount = float(values['amount'])
        signature = wallet.sign_transaction(wallet.public_key, recipient,
                                            amount)
        success = blockchain.add_transaction(recipient,
                                             wallet.public_key,
                                             signature,
                                             amount)
        if success:
            response = {
                'message': 'Successfully added transaction.',
                'transaction': {
                    'sender': wallet.public_key,
                    'recipient': recipient,
                    'amount': amount,
                    'signature': signature
                },
                'funds': blockchain.get_balance()
            }
            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Creating a transaction failed.'
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class MineTransactionsAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        if blockchain.resolve_conflicts:
            message = 'Resolve conflicts first, block not added!'
            return Response({'error': {
                'code': 409,
                'message': message
            }}, status=status.HTTP_409_CONFLICT)
        block = blockchain.mine_block()
        if block is not None:
            dict_block = block.__dict__.copy()
            dict_block['transactions'] = [
                tx.__dict__ for tx in dict_block['transactions']]
            response = {
                'message': 'Block added successfully.',
                'block': dict_block,
                'funds': blockchain.get_balance()
            }
            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': {
                'code': 500,
                'message': 'Adding a block failed.',
                'wallet_set_up': wallet.public_key is not None
            }}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ResolveConflictsAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        replaced = blockchain.resolve()
        if replaced:
            response = {'message': 'Chain was replaced!'}
        else:
            response = {'message': 'Local chain kept!'}
        return Response(response, status=status.HTTP_200_OK)


class GetOpenTransactionAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        transactions = blockchain.get_open_transactions()
        dict_transactions = [tx.__dict__ for tx in transactions]
        return Response(dict_transactions, status=status.HTTP_200_OK)


class GetChainAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        chain_snapshot = blockchain.chain
        dict_chain = [block.__dict__.copy() for block in chain_snapshot]
        for dict_block in dict_chain:
            dict_block['transactions'] = [
                tx.__dict__ for tx in dict_block['transactions']]
        return Response(dict_chain, status=status.HTTP_200_OK)


class AddNodeAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        values = request.data
        if not values:
            return Response({'error': {
                'code': 400,
                'message': 'No data attached.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        if 'node' not in values:
            response = {'message': 'No node data found.'}
            return Response({'error': {
                'code': 400,
                'message': 'No node data found.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        node = values['node']
        blockchain.add_peer_node(node)
        response = {
            'message': 'Node added successfully.',
            'all_nodes': blockchain.get_peer_nodes()
        }
        return Response(response, status=status.HTTP_201_CREATED)


class RemoveNodeAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def delete(self, request, node_url):
        if node_url == '' or node_url is None:
            return Response({'error': {
                'code': 400,
                'message': 'No node found.'
            }}, status=status.HTTP_400_BAD_REQUEST)
        blockchain.remove_peer_node(node_url)
        response = {
            'message': 'Node removed',
            'all_nodes': blockchain.get_peer_nodes()
        }
        return Response(response, status=status.HTTP_200_OK)


class GetNodesAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        nodes = blockchain.get_peer_nodes()
        response = {'all_nodes': nodes}
        return Response(response, status=status.HTTP_200_OK)
