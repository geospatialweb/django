from django.apps import AppConfig


class NodeapiConfig(AppConfig):
    name = 'nodeapi'
