import sys
# Return node_id using the active server port as a surrogate
def get_node_id():
    if '0.0.0.0:' in sys.argv[-1]:
        node_id = sys.argv[-1].split(':')[1]
    else:
        node_id = '8000'
    return node_id
