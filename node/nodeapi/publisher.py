import os
import pika


def publish_message(message):
    """Publishes message to the peer network.

    Arguments:
        :message: The body of the message.
    """
    # Parse CLODUAMQP_URL
    url = os.environ.get('jellyfish-01.rmq.cloudamqp.com',
                         'amqp://dfexzxjt:rIgwt9mW6mtYtg2ixaeHWMNvaU7NOjk_@jellyfish.rmq.cloudamqp.com/dfexzxjt')
    params = pika.URLParameters(url)
    params.socket_timeout = 5
    # Connect to CloudAMQP
    connection = pika.BlockingConnection(params)
    # Start a channel
    channel = connection.channel()
    # Declare a queue
    channel.queue_declare(queue='blockchain')
    # Publish message
    channel.basic_publish(exchange='', routing_key='blockchain', body=message)
    print('[x] Message sent to consumer')
    connection.close()
