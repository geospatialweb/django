import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import Home from './views/Home.vue'
import Network from './views/Network.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/network', component: Network }
  ]
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
